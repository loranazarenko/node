const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', (req, res, next) => {
    try{
      const users = UserService.getUsers();
      if (users) {
        res.status(200);
        res.data = users;
      }
    }catch (err) {
      res.status(400);
        res.err = err;
    } finally {
        next();
    }
    }, responseMiddleware);
  
  router.get('/:id', (req, res, next) => {
    try{
      const id = req.params.id;
      const found = UserService.search({ id });
      if (found) {
        res.status(200);
        res.data = found;
      }
    }catch (err) {
      res.status(400);
        res.err = err;
    } finally {
        next();
    }
    }, responseMiddleware);
  
  router.post('/', createUserValid, (req, res, next) => {
    try{
      const valid = req.user;
      if (valid) {
        res.status(200);
        res.data = valid;
      }
    }catch (err) {
      res.status(400);
        res.err = err;
    } finally {
        next();
    }
    }, responseMiddleware);
  
  router.put('/:id', updateUserValid, (req, res, next) => {
    try{
      const id = req.params.id;
      const userInfo = req.body;
      const updated = UserService.update(id, userInfo);
      if (updated) {
        res.status(200);
        res.data = updated;
      }
    }catch (err) {
      res.status(400);
        res.err = err;
    } finally {
        next();
    }
  }, responseMiddleware);
  
  router.delete('/:id', (req, res, next) => {
    try{
      const id = req.params.id;
      const deleted = UserService.remove(id);
      if (deleted) {
        res.status(200);
        res.data = deleted;
      }
    }catch (err) {
      res.status(400);
        res.err = err;
    } finally {
        next();
    }
  }, responseMiddleware);

module.exports = router;