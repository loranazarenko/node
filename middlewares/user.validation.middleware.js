const { user } = require("../models/user");
const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation
  const { id, firstName, lastName, email, phoneNumber, password } = req.body;
  try {
    if (id) {
      throw new Error("id must not be!");
    }
    if (!firstName || !lastName) {
      throw new Error("FirstName or LastName is wrong!");
    }

    if (!email || !email.includes("@gmail.com")) {
      throw new Error("Email is wrong!");
    }

    if (!phoneNumber) {
      throw new Error("Number is wrong");
    }

    if (!password || !password.length >= 3) {
      throw new Error("Password is wrong");
    }

    if (Object.keys(req.body).length == 0) {
      throw new Error("Data is wrong");
    }

    res.status(200).json({
      user: ({ firstName, lastName, phoneNumber, email, password } = body),
    });
    
    next();
  } catch (e) {
    res.status(400).json(e.message);
  }
};

const userKeys = Object.keys(user);

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  try {
    const { id } = req.params;
    const props = Object.keys(req.body);

    if (
      !props.every((idx) => id || userKeys.indexOf(idx) >= 0) ||
      !UserService.search(id) ||
      Object.keys(props).length == 0
    ) {
      throw new Error("Validation is wrong");
    } else {
      for (let idx in req.body) {
        if (idx === "email" && !req.body[idx].includes("@gmail.com")) {
          throw new Error("Validation is wrong");
        }
        if (idx === "password" && req.body[idx].length < 3) {
          throw new Error("Validation or data processing error");
        }
      }
      next();
    }
  } catch (e) {
    res.status(400).json(e.message);
  }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
