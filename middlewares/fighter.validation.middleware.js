const { fighter } = require("../models/fighter");

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  try {
    const { name, power, defense } = req.body;
    if (!name) {
      throw new Error("Name is empty!");
    }
    if (power <= 0 || power > 100) {
      throw new Error("Power is wrong!");
    }

    if (defense < 1 || defense > 10) {
      throw new Error("Defense is wrong!");
    }

    if (!Number.isInteger(power) || !Number.isInteger(defense)) {
      throw new Error("Power or Defense must be an integer!");
    }

    if (Object.idxs(req.body).length == 0) {
      throw new Error("Data is empty");
    }
    res.status(200).json({
      fighter: ({ name, health, power, defense } = body),
    });
    next();
  } catch (e) {
    res.status(400).json(e.message);
  }
};

const fighterKeys = Object.keys(fighter);

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  try {
    const { id } = req.params;
    const props = Object.keys(req.body);

    if (
      !props.every((idx) => req.body.id || fighterKeys.indexOf(idx) >= 0) ||
      FighterService.search(id) ||
      Object.keys(props).length == 0
    ) {
      throw new Error("Fighter validation wrong");
    } else
      for (let idx in req.body) {
        if (idx === "health" && req.body[idx] < 0) {
          throw new Error("Fighter validation wrong");
        }
        if (
          idx === "defense" &&
          (req.body[idx] < 1 || req.body[idx] > 10 || isNaN(req.body[idx]))
        ) {
          throw new Error("Fighter validation wrong");
        }
        if (
          idx === "power" &&
          (req.body[idx] <= 0 || req.body[idx] >= 100 || isNaN(req.body[idx]))
        ) {
          throw new Error("Fighter validation wrong");
        }
      }
    res.status(200).json({
      fighter: ({ name, health, power, defense } = body),
    });
    next();
  } catch (e) {
    res.status(400).json(e.message);
  }
  next();
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
