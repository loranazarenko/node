const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            throw Error('User not found');
        }
        return item;
    }
    createUser(data) {
        return  UserRepository.create(data);
   }

   updateUser(id, data) {
       return  UserRepository.update(id, data);
   }

   deleteUser(id) {
      return UserRepository.delete(id);
   }
    
}

module.exports = new UserService();